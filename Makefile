interface.xhtml: interface.rdf rdf2html.xsl schemas/xhtml-strict.xsd
	# validate rdf by parsing with rdfproc, may use better validator later
	rdfproc tmp parse interface.rdf || rm tmp-??2?.db
	rm tmp-??2?.db
	# convert rdf interface to human readable html
	xsltproc rdf2html.xsl interface.rdf > tmp.xhtml
	# check that the xhtml is valid
	xmllint --nonet --noout --schema schemas/xhtml-strict.xsd tmp.xhtml
	# move tmp.xhtml to c14n interface.xhtml
	XML_CATALOG_FILES=schemas/catalog.xml xmllint --nonet --nsclean --c14n11 tmp.xhtml > interface.xhtml

clean:
	rm -f interface.xhtml
