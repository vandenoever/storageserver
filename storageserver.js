/*global require: true, console: true*/
var fs = require('fs'),
    path = require('path'),
    http = require('http'),
    URL = require('url'),
    crypto = require('crypto'),
    sqlite3 = require('sqlite3').verbose(),
    serverport = 1917;

/**
 * @constructor
 * @param {!string} basedir
 * @param {!string} dbpath
 */
var StorageServer = (function () {
    /**
     * @param {!string} hashMethod
     * @param {!number} contentLength
     * @return {!Hash}
     */
    function createHasher(hashMethod, contentLength) {
        var hasher;
        if (hashMethod === "sha1") {
            hasher = crypto.createHash('sha1');
        } else if (hashMethod === "gitblobsha1") {
            hasher = crypto.createHash('sha1');
            hasher.update("blob ");
            hasher.update(contentLength.toString());
            hasher.update('\u0000');
        } else if (hashMethod === "gittreesha1") {
            hasher = crypto.createHash('sha1');
            hasher.update("tree ");
            hasher.update(contentLength.toString());
            hasher.update('\u0000');
        } else if (hashMethod === "gitcommitsha1") {
            hasher = crypto.createHash('sha1');
            hasher.update("commit ");
            hasher.update(contentLength.toString());
            hasher.update('\u0000');
        } else {
            throw "Unsupported hash method '" + hashMethod + "'";
        }
        return hasher;
    }
    /**
     * @param {!Array.<!string>} hashMethods
     * @param {!number} contentLength
     * @return {!Array.<!Object>}
     */
    function createHashers(hashMethods, contentLength) {
        var hashes = [], i;
        hashes.length = hashMethods.length;
        for (i = 0; i < hashes.length; i += 1) {
            hashes[i] = createHasher(hashMethods[i], contentLength);
        }
        return hashes;
    }
    /**
     * @param {!sqlite3.Database} db
     * @return {undefined}
     */
    function initializeDatabase(db) {
        db.run("CREATE TABLE IF NOT EXISTS hashmap (hash TEXT PRIMARY KEY, hashtype TEXT, sha1 TEXT, UNIQUE (hash, hashtype) ON CONFLICT FAIL, UNIQUE (hash, hashtype, sha1) ON CONFLICT FAIL)");
    }
    /**
     * @param {!sqlite3.Database} db
     * @param {!Object<!string,!string>} hashes
     * @param {!function} callback
     * @return {undefined}
     */
    function addHashesToDatabase(db, hashes, callback) {
        var stmt = db.prepare("INSERT INTO hashmap (hash, hashtype, sha1) VALUES (?, ?, ?)"),
            hashType,
            sha1 = hashes.sha1;
        for (hashType in hashes) {
            if (hashes.hasOwnProperty(hashType) && hashType !== "sha1") {
                console.log(hashType + " " + sha1);
                stmt.run(hashes[hashType], hashType, sha1);
            }
        }
        stmt.finalize(function () {
            callback();
        });
    }
    function writeFile(hashMethods, contentLength, instream, callback) {
        var path = Date.now().toString(),
            bytesRead = 0,
            hashers = createHashers(hashMethods, contentLength),
            out = fs.createWriteStream(path),
            ended = false;
        function abort() {
            out.end();
            fs.unlinkSync(path);
        }
        instream.on('data', function (d) {
            bytesRead += d.length;
            if (bytesRead > contentLength) {
                abort();
                return callback("Input stream is too long.");
            }
            var i;
            for (i = 0; i < hashers.length; i += 1) {
                hashers[i].update(d);
            }
        });
        instream.on('end', function () {
            ended = true;
            if (bytesRead !== contentLength) {
                abort();
                return callback("File size was not correct.");
            }
            var hashes = {};
            for (i = 0; i < hashers.length; i += 1) {
                hashes[hashMethods[i]] = hashers[i].digest('hex');
            }
            callback(null, path, hashes);
        });
        instream.on('close', function (d) {
            if (!ended) {
                abort();
                callback("Stream was closed unexpectedly.");
            }
        });
        instream.pipe(out);
    }
    function renameAndAddToDataBase(db, filepath, basedir, hashes, callback) {
        var sha1path = path.join(basedir, hashes.sha1);
        // save the file under the sha1 name and map other names
        // via database
        fs.exists(sha1path, function (exists) {
            if (exists) {
                fs.unlink(filepath);
                addHashesToDatabase(db, hashes, callback);
            } else {
                fs.rename(filepath, sha1path, function (err) {
                    fs.unlink(filepath);
                    if (err) {
                        fs.unlink(sha1path);
                        return callback(err);
                    }
                    addHashesToDatabase(db, hashes, callback);
                });
            }
        });
    }
    return function StorageServer(basedir, dbpath) {
        var self = this,
            db = new sqlite3.Database(dbpath);

        self.write = function (hashMethods, contentLength, instream, callback) {
            var hasSha1 = hashMethods.indexOf("sha1");
            if (!hasSha1) {
                hashMethods.push("sha1");
            }
            writeFile(hashMethods, contentLength, instream,
                function (error, filepath, hashes) {
                    if (error) {
                        return callback(error);
                    }
                    renameAndAddToDataBase(db, filepath, basedir, hashes,
                        function (err) {
                            callback(err, hashes);
                        });
                });
        };
        initializeDatabase(db);
    };
}());

function parseURL(url, hostname) {
    "use strict";
    if (!url) {
        return null;
    }
    var m;
    url = URL.parse(url, true);
    url.port = parseInt(url.port, 10) || 80;
    url.protocol = url.protocol || "http:";
    url.hostname = url.hostname || hostname;
    return url;
}

function post(request, response) {
    var length = request.headers['content-length'];
    console.log(length);
}

function put(storage, request, response) {
console.log(JSON.stringify(request.headers));
    var contentLength = request.headers && request.headers['content-length'],
        hashMethods = request.headers && request.headers['hash-methods'],
        ended = false,
        bytesRead = 0;
    if (contentLength === undefined) {
        throw "No content-length header provided.";
    }
    contentLength = parseInt(contentLength, 10);
    if (contentLength < 0) {
        throw "No valid content-length header provided.";
    }
    if (!hashMethods) {
        throw "No hash method provided.";
    }
    hashMethods = hashMethods.split(',');
    storage.write(hashMethods, contentLength, request,
        function (error, hashes) {
            if (error) {
                response.writeHead(200, error);
                return response.end();
            }
            response.writeHead(200);
            response.end();
        });
}

var storageserver = new StorageServer("data", "data.sqlite");

function handleRequest(url, request, response) {
    "use strict";
    if (request.method === "POST") {
        post(request, response);
        response.writeHead(500);
        response.end();
    } else if (request.method === "PUT") {
        put(storageserver, request, response);
    } else {
        response.writeHead(500);
        response.end();
    }
}

function server(request, response) {
    "use strict";
    var url = parseURL(request.url);
    handleRequest(url, request, response);
}
http.createServer(server).listen(serverport);
console.log("Started server on port " + serverport);
/*
var inp = fs.createReadStream('.git/objects/ce/4c5a00dafecc8e0dba5882c474a0738d4a765e'),
    z = zlib.createInflate(),
    out = fs.createWriteStream('out'),
    hash = crypto.createHash('sha1');
inp.on('end', function () {
    console.log(hash.digest('hex'));
});
z.on('data', function (d) {
    console.log(d);
    hash.update(d);
});
inp.pipe(z).pipe(out);
*/
